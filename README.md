# 2048-wasm

>> Yet another 2048 game

## Dev-Setup

Install [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/) via
```bash
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
```

## Credits

Inspired by [KarthikNedunchezhiyan/minesweeper](https://github.com/KarthikNedunchezhiyan/minesweeper/).
