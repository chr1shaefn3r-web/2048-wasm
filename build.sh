#!/bin/bash

wasm-pack build --target web --no-typescript --out-dir www/pkg

tar zcvf release.tar.gz pkg/* index.html style.css

