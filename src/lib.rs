use engine_2048_rs::game::ExternalFieldRepresentation;
use engine_2048_rs::game::Game;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct WasmGame {
    game: Game,
}

#[wasm_bindgen]
// Public methods to use from Javascript.
impl WasmGame {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            game: Game::default(),
        }
    }
    pub fn get_field(&self) -> ExternalFieldRepresentation {
        self.game.get_field()
    }
    pub fn get_score(&self) -> u64 {
        self.game.get_score()
    }
    pub fn move_down(&mut self) {
        self.game.move_down()
    }
    pub fn move_up(&mut self) {
        self.game.move_up()
    }
    pub fn move_left(&mut self) {
        self.game.move_left()
    }
    pub fn move_right(&mut self) {
        self.game.move_right()
    }
    pub fn is_game_over(&self) -> bool {
        self.game.is_game_over()
    }
}
